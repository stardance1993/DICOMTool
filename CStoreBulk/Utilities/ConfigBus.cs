﻿using CStoreBulk.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreBulk.Utilities
{
    public class ConfigBus
    {
        static readonly string PrintConfigFile = "PrinterSetting.json";
        static readonly string StoreSettingFile = "StoreSetting.json";
        static readonly string WorkListSettingFile = "WorkListSetting.json";
        static readonly string QRSettingFile = "QRSetting.json";

        public static PrinterSetting LoadPrintSetting()
        {
            if(File.Exists(PrintConfigFile))
            {
                var content = File.ReadAllText(PrintConfigFile);
                var entity = JsonConvert.DeserializeObject<PrinterSetting>(content);
                return entity;
            }
            return default(PrinterSetting);
        }

        public static StoreSetting LoadStoreSetting()
        {
            if (File.Exists(StoreSettingFile))
            {
                var content = File.ReadAllText(StoreSettingFile);
                var entity = JsonConvert.DeserializeObject<StoreSetting>(content);
                return entity;
            }
            return default(StoreSetting);
        }


        public static WorkListSetting LoadWorkListSetting()
        {
            if (File.Exists(WorkListSettingFile))
            {
                var content = File.ReadAllText(WorkListSettingFile);
                var entity = JsonConvert.DeserializeObject<WorkListSetting>(content);
                return entity;
            }
            return default(WorkListSetting);
        }

        public static QRSetting LoadQRSetting()
        { 
            if(File.Exists(QRSettingFile))
            {
                var content = File.ReadAllText(QRSettingFile);
                var entity = JsonConvert.DeserializeObject<QRSetting>(content);
                return entity;
            }
            return default(QRSetting);

        }

        public static bool SavePrintSetting(PrinterSetting sett)
        {
            var content = JsonConvert.SerializeObject(sett);
            File.WriteAllText(PrintConfigFile, content);
            return true;
        }

        public static bool SaveServerSetting(StoreSetting sett)
        {
            var content = JsonConvert.SerializeObject(sett);
            File.WriteAllText(StoreSettingFile, content);
            return true;
        }
    }
}
