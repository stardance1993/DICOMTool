﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreBulk.Utilities
{
    public class BuzEnum
    {
        public static List<String> FilmSizes = new List<string>
        {
            "8INX10IN",
            "8_5INX10IN",
            "10INX12IN",
            "14INX14IN",
            "14INX17IN",
            "A3",
            "A4"
        };
    }

    public enum FilmDestination
    {
        MAGAZINE,
        PROCESSOR,
        BIN_1,
        BIN_2,
        BIN_3
    }

    public enum FilmOrientation
    {
        PROTRAIT,
        LANDSCAPE
    }

    public enum MagnifacationType
    {
        REPLICATE,
        BILINEAR,
        CUBIC,
        NONE
    }
}
