﻿using Dicom.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreBulk.Utilities
{
    public class NetWatch
    {
        public async static Task<bool> Echo(string serverIP, int serverPort, string serverAET, string localAET)
        {
            var client = new Dicom.Network.Client.DicomClient(serverIP, serverPort, false, localAET, serverAET);
            var echoRequest = new DicomCEchoRequest();
            bool isEchoable = false;
            echoRequest.OnResponseReceived += (req, resp) =>
            {
                if (((DicomCEchoResponse)resp).Status.Code == 0)
                {
                    isEchoable = true;
                }
                else
                {
                    isEchoable = false;
                }
            };
            await client.AddRequestAsync(echoRequest);

            await client.SendAsync();
            return isEchoable;

            //Task sendTask = client.SendAsync();
            //if (sendTask.Wait(TimeSpan.FromSeconds(2)))
            //    return isEchoable;
            //else
            //    return false;
        }
    }
}
