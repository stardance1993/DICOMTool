﻿using CStoreBulk.Entities;
using CStoreBulk.Utilities;
using Dicom.Network;
using Dicom.Network.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CStoreBulk.Content
{
    /// <summary>
    /// SettingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class SettingWindow : Window,INotifyPropertyChanged
    {

        private static string StoreServerHost = string.Empty;
        private static int StoreServerPort = 0;
        private static string StoreServerAET = string.Empty;
        private static string LocalAET = string.Empty;

        private ObservableCollection<string> filmDests;

        private PrinterSetting printSettings;

        public ObservableCollection<string> FilmDests
        {
            get { return filmDests; }
            set { filmDests = value; }
        }

        private string selectedFilmDest;

        public string SelectedFilmDest
        {
            get { return selectedFilmDest; }
            set { selectedFilmDest = value; RaisePropertyChanged(nameof(SelectedFilmDest)); }
        }

        private ObservableCollection<string> filmDirects;

        public ObservableCollection<string> FilmDirects
        {
            get { return filmDirects; }
            set { filmDirects = value; RaisePropertyChanged(nameof(FilmDirects)); }
        }

        private string fileDirect;

        public string SelectedFilmDirect
        {
            get { return fileDirect; }
            set { fileDirect = value; RaisePropertyChanged(nameof(SelectedFilmDirect)); }
        }

        private ObservableCollection<string> filmSizes;

        public ObservableCollection<string> FilmSizes
        {
            get { return filmSizes; }
            set { filmSizes = value; }
        }

        private string filmSize;

        public string SelectedFilmSize
        {
            get { return filmSize; }
            set { filmSize = value; RaisePropertyChanged(nameof(SelectedFilmSize)); }
        }

        //private ObservableCollection<string> magTypes;

        //public ObservableCollection<string> MagTypes
        //{
        //    get { return magTypes; }
        //    set { magTypes = value; }
        //}

        private List<string> magTypes;

        public List<string> MagTypes
        {
            get { return magTypes; }
            set { magTypes = value; RaisePropertyChanged(nameof(MagTypes)); }
        }




        private string magType;

        public event PropertyChangedEventHandler PropertyChanged;

        public string SelectedMagType
        {
            get { return magType; }
            set { magType = value; RaisePropertyChanged(nameof(SelectedMagType)); }
        }





        public SettingWindow()
        {
            InitializeComponent();
            this.Loaded += SettingWindow_Loaded;
            this.DataContext = this;
        }

        private void SettingWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if(File.Exists("StoreSetting.json"))
            {
                var storeSetting = ConfigBus.LoadStoreSetting();
                this.ServerIP.Text = storeSetting.ServerIP;
                this.ServerPort.Text = storeSetting.ServerPort.ToString();
                this.ServerAE.Text = storeSetting.ServerAET;
                this.LocalAE.Text = storeSetting.LocalAET;
            }

            if (File.Exists("PrinterSetting.json"))
            {
                printSettings = ConfigBus.LoadPrintSetting();
                SelectedFilmDest = printSettings.FilmDestination;
                SelectedFilmDirect = printSettings.FilmDirection;
                SelectedFilmSize = printSettings.FilmSize;
                SelectedMagType = printSettings.MagnificationType;
            }

            if(File.Exists("WorkListSetting.json"))
            {
                var worklistSetting = ConfigBus.LoadWorkListSetting();
                this.WLServerIP.Text = worklistSetting.ServerIP;
                this.WLServerPort.Text = worklistSetting.ServerPort.ToString();
                this.WLServerAE.Text = worklistSetting.ServerAET;
                this.WLLocalAE.Text = worklistSetting.LocalAET;
            }
            if(File.Exists("QRSetting.json"))
            {
                var qrSetting = ConfigBus.LoadQRSetting();
                this.QRServerIP.Text = qrSetting.ServerIP;
                this.QRServerPort.Text = qrSetting.ServerPort.ToString();
                this.QRServerAE.Text = qrSetting.ServerAET;
                this.QRLocalAE.Text = qrSetting.LocalAET;
            }

            FilmDests = new ObservableCollection<string>();
            FilmDirects = new ObservableCollection<string>();
            FilmSizes = new ObservableCollection<string>();
            //MagTypes = new ObservableCollection<string>();
            MagTypes = new List<string>();
            BuzEnum.FilmSizes.ForEach(p => FilmSizes.Add(p));
            Enum.GetNames(typeof(FilmDestination)).ToList().ForEach(p => FilmDests.Add(p));
            Enum.GetNames(typeof(FilmOrientation)).ToList().ForEach(p => FilmDirects.Add(p));
            Enum.GetNames(typeof(MagnifacationType)).ToList().ForEach(p => MagTypes.Add(p));
            RaisePropertyChanged(nameof(FilmDests));
            RaisePropertyChanged(nameof(FilmDirects));
            RaisePropertyChanged(nameof(FilmSizes));

            


        }

        /// <summary>
        /// echo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(this.ServerPort.Text))
            {
                MessageBox.Show("端口不能为空");
                return;
            }
            StoreServerHost = this.ServerIP.Text;
            StoreServerPort = Convert.ToInt32(this.ServerPort.Text);
            StoreServerAET = this.ServerAE.Text;
            LocalAET = this.LocalAE.Text;
            bool echoable = false;
            try
            {
                echoable = await NetWatch.Echo(StoreServerHost, StoreServerPort, StoreServerAET, LocalAET);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ECHO Error! \r\n" + ex.Message);
                return;
            }
            if(echoable)
            {
                MessageBox.Show("ECHO OK!");
            }
            else
            {
                MessageBox.Show("ECHO Failed!");
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var storeSetting = new StoreSetting
            {
                ServerIP = this.ServerIP.Text,
                ServerPort = Convert.ToInt32(this.ServerPort.Text),
                ServerAET = this.ServerAE.Text,
                LocalAET = this.LocalAE.Text
            };
            string seriString = JsonConvert.SerializeObject(storeSetting);
            File.WriteAllText("StoreSetting.json", seriString);
        }

        private void Printer_Save_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(SelectedFilmDest) ||
               string.IsNullOrEmpty(SelectedFilmDirect) ||
               string.IsNullOrEmpty(SelectedFilmSize) ||
               string.IsNullOrEmpty(SelectedMagType))
            {
                MessageBox.Show("完善所有设置然后保存。");
            }
            else
            {
                PrinterSetting sett = new PrinterSetting()
                {
                    FilmDestination = SelectedFilmDest,
                    FilmDirection = SelectedFilmDirect,
                    MagnificationType = SelectedMagType,
                    FilmSize = SelectedFilmSize
                };
                var seriStr = JsonConvert.SerializeObject(sett);
                File.WriteAllText("PrinterSetting.json", seriStr);
                MessageBox.Show("保存完成！");
            }
        }

        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void WLbtnSave_Click(object sender, RoutedEventArgs e)
        {
            var worklistSetting = new WorkListSetting
            {
                ServerIP = this.WLServerIP.Text,
                ServerPort = Convert.ToInt32(this.WLServerPort.Text),
                ServerAET = this.WLServerAE.Text,
                LocalAET = this.WLLocalAE.Text
            };
            string seriString = JsonConvert.SerializeObject(worklistSetting);
            File.WriteAllText("WorkListSetting.json", seriString);
        }

        /// <summary>
        /// worklist echo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrEmpty(this.ServerPort.Text))
            //{
            //    MessageBox.Show("端口不能为空");
            //    return;
            //}
            string workListHost = this.WLServerIP.Text;
            int workListPort = Convert.ToInt32(this.WLServerPort.Text);
            string workListRemoteAE = this.WLServerAE.Text;
            string workListLocalAET = this.WLLocalAE.Text;
            bool echoable = false;
            try
            {
                echoable = await NetWatch.Echo(workListHost, workListPort, workListRemoteAE, workListLocalAET);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ECHO Error! \r\n" + ex.Message);
                return;
            }
            if (echoable)
            {
                MessageBox.Show("ECHO OK!");
            }
            else
            {
                MessageBox.Show("ECHO Failed!");
            }
        }

        //QR Service Echo
        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string QRServerIP = this.QRServerIP.Text;
            int QRPort = Convert.ToInt32(this.QRServerPort.Text);
            string QRServerAE = this.QRServerAE.Text;
            string QRLocalAET = this.QRLocalAE.Text;
            bool echoable = false;
            try
            {
                echoable = await NetWatch.Echo(QRServerIP, QRPort, QRServerAE, QRLocalAET);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ECHO Error! \r\n" + ex.Message);
                return;
            }
            if (echoable)
            {
                MessageBox.Show("ECHO OK!");
            }
            else
            {
                MessageBox.Show("ECHO Failed!");
            }
        }

        //QR Server Setting Save
        private void QRbtnSave_Click(object sender, RoutedEventArgs e)
        {
            var QRSetting = new QRSetting
            {
                ServerIP = this.QRServerIP.Text,
                ServerPort = Convert.ToInt32(this.QRServerPort.Text),
                ServerAET = this.QRServerAE.Text,
                LocalAET = this.QRLocalAE.Text
            };
            string seriString = JsonConvert.SerializeObject(QRSetting);
            File.WriteAllText("QRSetting.json", seriString);
        }
    }
}
