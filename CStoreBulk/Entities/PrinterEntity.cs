﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreBulk.Entities
{
    class PrinterEntity
    {
    }

    public struct PrinterSetting
    {
        public string FilmDestination { get; set; }

        public string FilmDirection { get; set; }

        public string MagnificationType { get; set; }

        public string FilmSize { get; set; }

    }
}
