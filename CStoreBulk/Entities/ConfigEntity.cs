﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreBulk.Entities
{
    class ConfigEntity
    {

    }

    /// <summary>
    /// 服务端设置
    /// </summary>
    public struct StoreSetting
    {
        public string ServerIP { get; set; }

        public int ServerPort { get; set; }

        public string ServerAET { get; set; }

        public string LocalAET { get; set; }
    }

    public struct WorkListSetting
    {
        public string ServerIP { get; set; }

        public int ServerPort { get; set; }

        public string ServerAET { get; set; }

        public string LocalAET { get; set; }
    }

    public struct QRSetting
    {
        public string ServerIP { get; set; }

        public int ServerPort { get; set; }

        public string ServerAET { get; set; }

        public string LocalAET { get; set; }
    }
}
